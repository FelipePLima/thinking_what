require 'rails_helper'

RSpec.describe Follower, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:follower_user).class_name('User').with_foreign_key(:follower_user_id) }
    it { is_expected.to have_many(:posts).through(:follower_user) }
  end
end
