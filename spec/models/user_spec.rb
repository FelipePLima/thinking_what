require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:posts).dependent(:destroy) }
    it { is_expected.to have_many(:followers).with_foreign_key(:follower_user_id).dependent(:destroy) }
    it { is_expected.to have_many(:followed).class_name('Follower').with_foreign_key(:user_id) }
  end

  describe '#followed_for?' do
    let(:user) { create(:user, email: 'roberto@gmail.com.br') }
    let(:user_follower) { create(:user, email: 'felipe@gmail.com.br') }
    let(:user_not_follower) { create(:user, email: 'paulo@gmail.com.br') }
    let!(:follower) { create(:follower, follower_user_id: user_follower.id, user: user) }

    it {expect(user_follower.followed_for? user).to eq(true)}
    it {expect(user_not_follower.followed_for? user).to eq(false)}
  end
end
