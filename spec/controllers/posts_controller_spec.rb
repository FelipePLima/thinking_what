require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  describe "GET #new" do
    context 'when user is logged in' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      before do
        sign_in user
        get :new
      end
      it { expect(assigns(:post)).to be_a_instance_of(Post) }
      it { expect(response).to be_success }
    end

    context 'when user is logged out' do
      before do
        get :new
      end

      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end

  describe "POST #create" do
    let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
    before do
      sign_in user
      do_action
    end

    context 'when post create with success' do
      def do_action
        post :create, params: { post: FactoryGirl.attributes_for(:post) }
      end

      it { expect(response).to redirect_to profiles_index_path(user.id) }
      it { expect(assigns(:post)).to be_a_instance_of(Post) }
      it { expect(user.reload.posts.count).to eq(1) }
      it { expect(flash[:notice]).to eq("Post criado com sucesso.") }
    end

    context 'when post not create' do
      def do_action
        post :create, params: { post: {comment: nil, user_id: user.id } }
      end

      it { expect(response).to render_template :new }
    end
  end

  describe "GET #edit" do
    context 'when user is logged in' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:post) { create(:post, comment: 'your post.', user: user) }
      before do
        sign_in user
        get :edit, params: { id: post.id }
      end

      it { expect(assigns(:post)).to be_a_instance_of(Post) }
      it { expect(assigns(:post)).to eq(post) }
      it { expect(response).to be_success }
    end

    context 'when user try update post belongs to other user' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:other_user) { create(:user, email: 'felipe@gmail.com.br') }
      let!(:post) { create(:post, comment: 'your post.', user: user) }
      before do
        sign_in other_user
        get :edit, params: { id: post.id }
      end

      it { expect(response).to redirect_to root_path }
      it { expect(flash[:alert]).to eq("Você não tem permissão, para esta ação!") }
    end
  end

  describe "PUT #update" do
    context 'when post updated with success' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:post) { create(:post, comment: 'your post.', user: user) }
      before do
        sign_in user
        do_action
      end
      def do_action
        patch :update, params: { id: post.id, post: {comment: 'My post Updated.', user: user} }
      end

      it{ expect(assigns(:post).comment).to eq('My post Updated.') }
      it{ expect(flash[:notice]).to eq('Post atualizado com sucesso.') }
      it{ expect(response).to redirect_to profiles_index_path(user.id) }
    end

    context 'when post no updated' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:post) { create(:post, comment: 'your post.', user: user) }
      before do
        sign_in user
        do_action
      end
      def do_action
        patch :update, params: { id: post.id, post: {comment: '', user: user} }
      end

      it { expect(response).to render_template :edit }
    end

    context 'when user try update post belongs to other user' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:other_user) { create(:user, email: 'felipe@gmail.com.br') }
      let!(:post) { create(:post, comment: 'your post.', user: user) }
      before do
        sign_in other_user
        do_action
      end
      def do_action
        patch :update, params: { id: post.id, post: {comment: '', user: user} }
      end

      it { expect(response).to redirect_to root_path }
      it { expect(flash[:alert]).to eq("Você não tem permissão, para esta ação!") }
    end
  end
end
