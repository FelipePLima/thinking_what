require 'rails_helper'

RSpec.describe ProfilesController, type: :controller do

  describe "GET #home" do
    let!(:user_followed) { create(:user, email: 'felipe@gmail.com.br') }
    let!(:user) { create(:user, email: 'renato@gmail.com.br') }
    let!(:follower) { create(:follower, user_id: user.id, follower_user_id: user_followed.id) }
    let!(:post) { create(:post, comment: 'Oi todo mundo.', user_id: user_followed.id) }

    before do
      sign_in user
      get :home
    end

    it { expect(response).to have_http_status(:success) }
    it { expect(assigns(:posts)).to eq([post]) }
  end

  describe "GET #index" do
    let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
    let!(:post) { create(:post, comment: 'Testando', user_id: user.id) }

    before do
      sign_in user
      get :index, params: { id: user.id }
    end

    it { expect(response).to have_http_status(:success) }
    it { expect(assigns(:posts)).to eq([post]) }
  end
end
