require 'rails_helper'

RSpec.describe FollowersController, type: :controller do
  describe "GET #follow" do
    context 'when user is logged in' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:user_follower) { create(:user, email: 'felipe@gmail.com.br') }
      before do
        sign_in user
        get :follow, params: { id: user_follower.id }
      end
      it { expect(user_follower.followers.last.user).to eq(user) }
      it { expect(flash[:notice]).to eq("Você está conectado com este usuário!") }
    end

    context 'when user is logged out' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      before do
        get :follow, params: { id: user.id }
      end

      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end

  describe "GET #unfollow" do
    context 'when user is logged in' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      let!(:user_follower) { create(:user, email: 'felipe@gmail.com.br') }
      let!(:follower) { create(:follower, user_id: user.id, follower_user_id: user_follower.id) }
      before do
        sign_in user
        get :unfollow, params: { id: user_follower.id }
      end
      it { expect(user_follower.followers).to eq([]) }
      it { expect(flash[:notice]).to eq("Você deixou de seguir este usuário!") }
    end

    context 'when user is logged out' do
      let!(:user) { create(:user, email: 'roberto@gmail.com.br') }
      before do
        get :follow, params: { id: user.id }
      end

      it { expect(response).to redirect_to(new_user_session_path) }
    end
  end
end
