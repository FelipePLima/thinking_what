require 'rails_helper'

RSpec.describe CustomMailer, type: :mailer do
  let(:user) { create(:user, email: 'felipe@gmail.com.br') }
  let(:send_user) { create(:user, email: 'roberto@gmail.com.br') }

  describe 'follower' do
    let(:mail) { CustomMailer.follow_notification(user, send_user.id) }

    it 'renders the headers' do
      expect(mail.subject).to eq('Você tem um Novo Seguidor!')
      expect(mail.to).to eq([send_user.email])
      expect(mail.from).to eq(['felipethinkingwhat@gmail.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Você está sendo seguido por')
    end
  end
end
