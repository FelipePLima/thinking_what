Rails.application.routes.draw do
  root 'profiles#home'

  get 'profiles/:id/index', controller: 'profiles', action: 'index', as: 'profiles/index'
  get 'profiles/users_search'

  get 'followers/:id/follow', controller: 'followers', action: 'follow', as: 'followers/follow'
  get 'followers/:id/unfollow', controller: 'followers', action: 'unfollow', as: 'followers/unfollow'

  devise_for :users
  resources :posts
end
