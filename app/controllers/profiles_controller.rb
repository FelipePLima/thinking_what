class ProfilesController < ApplicationController

  def home
    if current_user && current_user.followers
      @posts = current_user.followed.map{|f| f.posts.includes(:user)}.flatten
    end
  end

  def index
    @user = params[:id].present? ? User.find(params[:id]) : current_user
    @posts = @user.posts.order(created_at: :desc)
  end

  def users_search
    @users = User.where("email LIKE '%#{params[:email]}%'")
  end
end
