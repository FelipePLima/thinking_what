class PostsController < ApplicationController
  before_action :set_post, only: [:edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :require_permission, only: [:edit, :update]

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = current_user.posts.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to profiles_index_path(current_user.id), notice: 'Post criado com sucesso.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to profiles_index_path(current_user.id), notice: 'Post atualizado com sucesso.' }
      else
        format.html { render :edit }
      end
    end
  end

  private
    def require_permission
      if current_user != Post.find(params[:id]).user
        flash[:alert] = "Você não tem permissão, para esta ação!"
        redirect_to root_path
      end
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:comment, :user_id)
    end
end
