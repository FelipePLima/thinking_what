class FollowersController < ApplicationController
  before_action :authenticate_user!

  def follow
    follower = Follower.new(user_id: current_user.id, follower_user_id: params[:id])
    respond_to do |format|
      if follower.save
        CustomMailer.follow_notification(current_user, params[:id]).deliver_now
        format.html { redirect_to profiles_index_path(params[:id]), notice: 'Você está conectado com este usuário!' }
      else
        format.html { redirect_to profiles_index_path(params[:id])}
      end
    end
  end

  def unfollow
    user = User.find(params[:id])
    follower = user.followers.where(user_id: current_user.id).first
    respond_to do |format|
      if follower.destroy
        format.html { redirect_to profiles_index_path(user.id), notice: 'Você deixou de seguir este usuário!' }
      else
        format.html { redirect_to profiles_index_path(user.id) }
      end
    end
  end
end
