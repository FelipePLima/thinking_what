class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts, dependent: :destroy
  has_many :followers, foreign_key: :follower_user_id, dependent: :destroy
  has_many :followed,  class_name: 'Follower', foreign_key: :user_id

  def followed_for? user
    self.followers.map{|f| f.user}.include? user
  end
end
