class Follower < ApplicationRecord
  belongs_to :user, foreign_key: :user_id
  belongs_to :follower_user, class_name: 'User', foreign_key: :follower_user_id
  has_many :posts, through: :follower_user
end
