class ApplicationMailer < ActionMailer::Base
  default from: 'felipethinkingwhat@gmail.com'
  layout 'mailer'
end
