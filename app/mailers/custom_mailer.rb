class CustomMailer < ApplicationMailer

  def follow_notification(current_user, user_followed_id)
    @current_user = current_user
    @user_followed = User.find(user_followed_id)
    mail(to: @user_followed.email, subject: 'Você tem um Novo Seguidor!')
  end

end
