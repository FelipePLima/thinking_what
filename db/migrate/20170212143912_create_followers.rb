class CreateFollowers < ActiveRecord::Migration[5.0]
  def change
    create_table :followers do |t|
      t.references :follower_user, references: :users, index: true
      t.references :user, references: :users, index: true
      t.timestamps
    end
  end
end
